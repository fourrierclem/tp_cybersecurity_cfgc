Noms des participants au projet : 

Clément Fourrier / Guillaume Charpentier

Le projet à été réalisé en Symfony avec une base de données Postgre.
Si vous souhaitez le tester, n'oubliez pas les manipulations du type :

- "composer install" 
- modification du ".env"
- etc ..

Nous avons repris l'interface de connexion de Facebook.

![](https://gitlab.com/fourrierclem/tp_cybersecurity_cfgc/-/raw/3c926cef02187f0f0208efe9964c76124e249cd9/ressources/fb.png)

Les credentials sont stockés en base de données, le mot de passe est haché.

![](https://gitlab.com/fourrierclem/tp_cybersecurity_cfgc/-/raw/master/ressources/pwd.png)

Le token d'authentification est visible dans les cookies via la console.

![](https://gitlab.com/fourrierclem/tp_cybersecurity_cfgc/-/raw/master/ressources/token.png)


Nous vous avions déjà fait une présentation en privée sur Teams, mais si vous avez la moindre question, n'hesitez pas à nous contacter à cette adresse : _clement.fourrier@campus.academy_

**Merci !**

