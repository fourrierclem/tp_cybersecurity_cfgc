<?php

namespace App\Controller;

use App\Security\TokenAuthenticator;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authentication\AuthenticationManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class AdminController extends AbstractController
{
    /**
     * @Route("/admin", name="admin")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(Security $security, Session $session, Request $request): Response
    {
        $userToken = $this->get('security.token_storage');
        //$ttoken = $security->getToken()->getSecret();
        //dump($ttoken);
        //dump($security->getToken()->getAttributes());
        $token = $request->headers->get('X-AUTH-TOKEN');
        dump($token);
        return $this->render('admin/index.html.twig');
    }
}