(function ($) {
    $(document).ready(function () {
        $('.select2entity[data-autostart="true"]').select2entity();
        $('.select2array[data-autostart="true"]').select2array();


        $( "body" ).on("click", "a.ajaxload", function( event ) 
        {
            event.preventDefault(); // replace default event
            //_showloading(); // show loading <div>
        //alert($(event.currentTarget).attr("target"));
           load($(event.currentTarget).attr("href"), $(event.currentTarget).data("window-title") , "" , $(event.currentTarget).attr("target"));
        });
        

    });
})(jQuery);


(function ($) {
    $.fn.select2array = function (options) {
        this.each(function () {
            let $s2 = $(this);
   

            $s2.select2({
                templateResult: function (d) { return $(d.text); },
                templateSelection: function (d) { return $(d.text); },
            });
        });
    };
        
})(jQuery);



/**
 * load : rechargement de zone <div></div>
 * @param 	url 			url
 * @param 	title 			optionel : titre du dialog
 * @param	data	    	tableau de param�tres [name:...,value:...]
 * @param	target	    	id div cible
 */	
function load(url, title, data, target){
	
    if(! (data instanceof Array)){
       var data = new Array();
    }	

    var targets = ["_parent", "_self","_top","_blank","_dialog"];  //<! target par d�faut : redirection

    if(! inArray(target, targets) && $("#" + target).length > 0 ){
        
        data.push({name: 'ajaxreload' , value: 1 });		//<! ajout des variables de rechargement pour PHP
        data.push({name: 'ajaxparts' , value: target });
        
        var definition = get_url(url, data); //<! r�cup�ration d�finition url
    
        $.ajax({ // ajax
            url: definition["url"], // url de la page � charger
            data: definition["data"] ,
            type: 'GET',
            cache: false, // pas de mise en cache
            success:function(html){ // si la requ�te est un succ�s
                if($('#' + target).length > 0) {
                    $('#' + target).html(html);	
                }else{
                    window.parent.$('#' + target).html(html);	
                }
            },
            error:function(XMLHttpRequest, textStatus, errorThrows){ // erreur durant la requete
            },
            complete:function(html) 
            {
                if(definition["tag"])
                {
                    $("#" + target).find("#" + definition["tag"]).select();
                }
              //  _hideloading();
            }
        });
    }else{     
         
         
        var definition = get_url(url, data); //<! r�cup�ration d�finition url
        
        if(target == "_parent"){
            window.parent.location.assign(definition["full"]);   				//<! Rechargement page parent
        }else if(target == "_dialog"){							
            load_dialog(definition["full"], title);                            //<! jQuery-ui dialog extended
        }else{	
            if(data["wid"] && dialog_isopen(data["wid"])){
                dialog_replace(data["wid"] , definition["full"]);  			 	//<! dialog ? : modification src=""
            }else{
                window.location.assign(definition["full"]);						//<! Rechargement page courante
            }
        }
    }	
}	

/**
 * inArray : �quivalent de la fonction PHP in_array
 * @param 	needle 			valeur � chercher
 * @param	haystack	    tableau de recherche
 */                            
function inArray(needle, haystack) {
    if($.isArray(needle)){
        for(var j = 0; j < needle.length; j++) {
            for(var i = 0; i < haystack.length; i++) {
                if(haystack[i] == needle[j]) return true;
            }
        }	
    }
    else
    {	
        for(var i = 0; i < haystack.length; i++) {
            if(haystack[i] == needle) return true;
        }
    }
    return false;
}

/**
 * get_url : retourne un tableau avec les �lements de href 
 * @param 	url 			url (avec / sans param�tres / tags...)
 * @param	data	    	tableau de param�tres [name:...,value:...]
 * @return	array			{["url"] = "fiche.php", ["data"]= "id=1&f=...", ...}
 */	
function get_url(url, data){
		
    if(! (data instanceof Array)){
       var data = new Array();
    }
    
    var arr_url 	= new Array();
    var arr_param 	= new Array();
    
    var arr_a = url.split("#");
    var arr_b = arr_a[0].split("?");
    
    var full_url = arr_b[0];
    arr_url["url"] = arr_b[0];      //<! url 	http://12.2.2.2/scrta
     
    if(arr_b[1]){	 
        arr_param.push(arr_b[1]);						//<! param�tres d\'url  
    }
    
    if(data.length > 0){                               //<! param�tres d\'objet data
        arr_param.push($.param(data));
    }
    
    if(arr_param.length > 0){
        full_url += "?" + arr_param.join("&");
        arr_url["data"] = arr_param.join("&");			//<! param id=1&ghf=c&...
    }
    
    if(arr_a[1]){
        full_url += "#" + arr_a[1];
        arr_url["tag"] = arr_a[1]; 		//<! tag 	#fk_soc	
    }
    arr_url["full"] = full_url; 		//<! url compl�te

    return arr_url; 
}

/**
 * dialog_load : Affichage dialog (jQuery-ui dialog)
  * @param 	url 			url
 * @param 	titre 			titre du dialogue
 */		
function load_dialog(url, title){
	
    var wid = new Date().getTime();
    
    if(! (data instanceof Array)){
           var data = new Array();
    }	
    data.push({name: 'wid' , value: wid });		//<! ajout identification du dialog
    
    var definition = get_url(url, data);
    
    var dialogOptions = {
        "title" : title,
        "width" : 1024,
        "height" : 768,
        "modal" : false,
        "resizable" : true,
        "draggable" : true
    };
    
    var dialogExtendOptions = {
        "closable" : true,
        "collapsable" : true,
        "maximizable" : true,
        "minimizable" : true,
        "dblclick" : "maximize"
        
    };
    window.parent.$('<div id="' + wid + '"><iframe id="f' + wid + '" src="' +  definition["url"] + '?' + definition["data"] + '" style="margin: 0; padding: 0; border: none; width: 100%;  height: 100%;scrolling:no;" /></div>').dialog(dialogOptions).dialogExtend(dialogExtendOptions);  		
}
    